// Path: Assets/Scripts/Networking/DataSyncController.cs

using Unity.Netcode;
using UnityEngine;

namespace Networking
{
    public class DataSyncController : NetworkBehaviour
    {
        [SerializeField] private Transform networkedHead, networkedHandL, networkedHandR;
        [SerializeField] private SkinnedMeshRenderer skinHandL, skinHandR;
        [SerializeField] private MeshRenderer meshHead;
        private Transform transLocalPlayerHead, transLocalHandL, transLocalHandR, transLocalPlayer;
        private bool hasLocalPlayerHead, hasLocalHandL, hasLocalHandR;

        public override void OnNetworkSpawn()
        {
            transLocalPlayerHead = GameObject.Find("Local Player Head")?.GetComponent<Transform>();
            transLocalHandL = GameObject.Find("LocalHandL")?.GetComponent<Transform>();
            transLocalHandR = GameObject.Find("LocalHandR")?.GetComponent<Transform>();
            transLocalPlayer = GameObject.Find("Player")?.GetComponent<Transform>();

            // in case user has no VR headset
            hasLocalPlayerHead = transLocalPlayerHead != null;
            hasLocalHandL = transLocalHandL != null;
            hasLocalHandR = transLocalHandR != null;

            if (IsLocalPlayer)
            {
                skinHandL.enabled = false;
                skinHandR.enabled = false;
                meshHead.enabled = false;
            }
        }

        private void Update()
        {
            if (IsLocalPlayer)
            {
                SyncTransforms();
            }
        }

        private void SyncTransforms()
        {
            var transformVar = transform;
            transformVar.position = transLocalPlayer.position;
            transformVar.rotation = transLocalPlayer.rotation;

            if (hasLocalPlayerHead)
            {
                networkedHead.position = transLocalPlayerHead.position;
                networkedHead.rotation = transLocalPlayerHead.rotation;
            }

            if (hasLocalHandL)
            {
                networkedHandL.position = transLocalHandL.position;
                networkedHandL.rotation = transLocalHandL.rotation;
            }

            if (hasLocalHandR)
            {
                networkedHandR.position = transLocalHandR.position;
                networkedHandR.rotation = transLocalHandR.rotation;
            }
        }
    }
}