using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class CenterScreenUIInteractExperimental : MonoBehaviour
    {
        [SerializeField] private GameObject virtualMousePrefab;
        [SerializeField] private float mouseDistance = 1f;
        [SerializeField] private LayerMask uiLayerMask;

        private GameObject _virtualMouse;
        private Ray _ray;
        private RaycastHit _hit;

        private void Start()
        {
            InitializeVirtualMouse();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                UpdateRaycast();
            }
        }

        private void InitializeVirtualMouse()
        {
            if (virtualMousePrefab == null)
            {
                Debug.LogError("VirtualMousePrefab is not assigned.");
                return;
            }

            var transformVar = transform;
            _virtualMouse = Instantiate(virtualMousePrefab, transformVar.position + transformVar.forward * mouseDistance,
                Quaternion.identity, transformVar);
        }

        private void UpdateRaycast()
        {
            _ray = new Ray(_virtualMouse.transform.position,
                _virtualMouse.transform.TransformDirection(Vector3.forward));
            Physics.Raycast(_ray, out _hit, 1000f, uiLayerMask);
            if (!_hit.collider) return;

            Debug.Log($"UI Hit: {_hit.collider.gameObject.name}");
            ExecuteEvents.Execute(_hit.collider.gameObject, new PointerEventData(EventSystem.current),
                ExecuteEvents.pointerClickHandler);

            InputField inputField = _hit.collider.gameObject.GetComponent<InputField>();
            if (inputField != null)
            {
                inputField.Select();
                inputField.ActivateInputField();
            }
        }
    }
}