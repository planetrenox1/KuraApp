using System;
using System.Threading.Tasks;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using Unity.Services.Vivox;
using UnityEngine;
using UnityEngine.UI;

namespace Test
{
    internal class TestNetworkingWithoutVR : MonoBehaviour
    {
        [SerializeField] private TMP_InputField inputFieldJoinCode;


        /// <summary>
        /// Event handler for when the Join button is clicked.
        /// </summary>
        public async void OnInvite()
        {
            //textJoinCode.text = "Please wait...";
            try
            {
                string joinCode = await StartHostWithRelay();
                if (!string.IsNullOrEmpty(joinCode))
                {
                    Debug.Log($"Host started. Join code: {joinCode}");
                    //textJoinCode.text = joinCode;
                }
                else
                {
                    Debug.LogError("Failed to start host.");
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error starting host: {ex.Message}");
            }
        }

        /// <summary>
        /// Event handler for when the Join button is clicked.
        /// </summary>
        public async void OnJoin()
        {
            string joinCode = inputFieldJoinCode.text;
            if (!string.IsNullOrEmpty(joinCode))
            {
                try
                {
                    bool success = await StartClientWithRelay(joinCode);
                    if (success)
                    {
                        Debug.Log("Client started and joined successfully.");
                    }
                    else
                    {
                        Debug.LogError("Failed to start client or join.");
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError($"Error joining game: {ex.Message}");
                }
            }
            else
            {
                Debug.LogError("Join code is empty.");
            }
        }


        /// <summary>
        /// Starts a game host with a relay allocation: it initializes the Unity services & Vivox, signs in anonymously and starts the host with a new relay allocation.
        /// </summary>
        /// <param name="maxConnections">Maximum number of connections to the created relay.</param>
        /// <returns>The join code that a client can use.</returns>
        /// <exception cref="ServicesInitializationException"> Exception when there's an error during services initialization </exception>
        /// <exception cref="UnityProjectNotLinkedException"> Exception when the project is not linked to a cloud project id </exception>
        /// <exception cref="CircularDependencyException"> Exception when two registered <see cref="IInitializablePackage"/> depend on the other </exception>
        /// <exception cref="AuthenticationException"> The task fails with the exception when the task cannot complete successfully due to Authentication specific errors. </exception>
        /// <exception cref="RequestFailedException"> See <see cref="IAuthenticationService.SignInAnonymouslyAsync"/></exception>
        /// <exception cref="ArgumentException">Thrown when the maxConnections argument fails validation in Relay Service SDK.</exception>
        /// <exception cref="RelayServiceException">Thrown when the request successfully reach the Relay Allocation service but results in an error.</exception>
        /// <exception cref="ArgumentNullException">Thrown when the UnityTransport component cannot be found.</exception>
        private async Task<string> StartHostWithRelay(int maxConnections = 5)
        {
            await UnityServices.InitializeAsync();
            if (!AuthenticationService.Instance.IsSignedIn)
            {
                await AuthenticationService.Instance.SignInAnonymouslyAsync();
            }

            await VivoxService.Instance.InitializeAsync();
            await VivoxService.Instance.LoginAsync();

            Allocation allocation = await RelayService.Instance.CreateAllocationAsync(maxConnections);
            NetworkManager.Singleton.GetComponent<UnityTransport>()
                .SetRelayServerData(new RelayServerData(allocation, "dtls"));
            string joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);
            await VivoxService.Instance.JoinGroupChannelAsync(joinCode, ChatCapability.AudioOnly);
            return NetworkManager.Singleton.StartHost() ? joinCode : null;
        }

        /// <summary>
        /// Joins a game with relay: it will initialize the Unity services & Vivox, sign in anonymously, join the relay with the given join code and start the client.
        /// </summary>
        /// <param name="joinCode">The join code of the allocation</param>
        /// <returns>True if starting the client was successful</returns>
        /// <exception cref="ServicesInitializationException"> Exception when there's an error during services initialization </exception>
        /// <exception cref="UnityProjectNotLinkedException"> Exception when the project is not linked to a cloud project id </exception>
        /// <exception cref="CircularDependencyException"> Exception when two registered <see cref="IInitializablePackage"/> depend on the other </exception>
        /// <exception cref="AuthenticationException"> The task fails with the exception when the task cannot complete successfully due to Authentication specific errors. </exception>
        /// <exception cref="RequestFailedException">Thrown when the request does not reach the Relay Allocation service.</exception>
        /// <exception cref="ArgumentException">Thrown if the joinCode has the wrong format.</exception>
        /// <exception cref="RelayServiceException">Thrown when the request successfully reach the Relay Allocation service but results in an error.</exception>
        /// <exception cref="ArgumentNullException">Thrown when the UnityTransport component cannot be found.</exception>
        private async Task<bool> StartClientWithRelay(string joinCode)
        {
            await UnityServices.InitializeAsync();
            if (!AuthenticationService.Instance.IsSignedIn)
            {
                await AuthenticationService.Instance.SignInAnonymouslyAsync();
            }

            await VivoxService.Instance.InitializeAsync();
            await VivoxService.Instance.LoginAsync();
            await VivoxService.Instance.JoinGroupChannelAsync(joinCode, ChatCapability.AudioOnly);

            var joinAllocation = await RelayService.Instance.JoinAllocationAsync(joinCode: joinCode);
            NetworkManager.Singleton.GetComponent<UnityTransport>()
                .SetRelayServerData(new RelayServerData(joinAllocation, "dtls"));
            return !string.IsNullOrEmpty(joinCode) && NetworkManager.Singleton.StartClient();
        }
    }
}