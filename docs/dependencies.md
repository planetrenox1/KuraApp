| Dependencies                                                                                          | Purpose                           |
|-------------------------------------------------------------------------------------------------------|-----------------------------------|
| Unity 2023.2.3f1 VR Template                                                                          | Required for Unity mppm           |
| com.unity.netcode.gameobjects                                                                         | Host/Client networking            |
| com.unity.services.relay                                                                              | Room join codes and IP protection |
| com.unity.services.vivox                                                                              | Voice comms                       |
| com.unity.multiplayer.playmode                                                                        | Multi-user testing                |
| github.com/siccity/gltfutility                                                                        | Importing gltf 3d models          |
| [link](https://sketchfab.com/3d-models/low-poly-hand-with-animation-33253439b0874d09b46a9a18685c863c) | Hand model                        |
| [link](https://sketchfab.com/3d-models/gal-gadot-wonder-woman-head-132cf81d3afe4153bc81ab157da2c884)  | Head model                        |
