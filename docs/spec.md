﻿# Product Spec. Document for a Multi-User Unity VR Project (Virtual-Desktop + GPT-4Vision)

## Project Overview, Motivation, and Market

OpenAI releases new API, GPT-4V(ision) - and surprisingly there aren't many viable use-cases found. We need an organic
and **free** use-case.

Most virtual desktops on Steam are paid. We could offer a virtual desktop for free. Use this opportunity to test GPT-4V
as a paid assistant.

**Trend:** currently products on Steam featuring anime are reviewed positively without trying to be exceptional.

This is due to `Moe anthropomorphism`(https://en.m.wikipedia.org/wiki/Moe_anthropomorphism). This is a form of
anthropomorphism in anime, manga, and games where moe qualities are given to non-human beings. (key. "endear")

We could use a very simple form of anthropomorphism to sell GPT-4V to the user by giving the virtual desktop assistant
real presence since this is in VR. (Currently most virtual desktops are in empty rooms. i.e. wasted space that could be
filled with a virtual assistant).

**Note**: we will allow other users to join our virtual desk using P2P and allow voice communication. For now our
assistant is deaf and only communicates through typing and vision.

<img src="images/2024-01-04.png" width="200" alt="assistant-chan">

## User Narratives

1. **Alex** A student who uses the VR desktop for studying. Prefers the anime-style assistant for company and
   light-hearted interactions while working.

2. **Jordan** A remote worker who uses the application for work prefers vision based help as opposed to copy-pasting
   everything manually.

## Disclaimer and Difficulties

Please note that due to the time constraint nothing will receive any polish that would normally be required for such an
application to be good. Testing multiplayer with unity is already difficult but a lot of extra time was wasted because I
couldn't test multiplayer on the same pc as having 1 vr headset connected would cause a crash. So much of the vr syncing
took extra time and this meant I couldn't add much interactivity.

## Compatibility, Requirements, and Time

| **Aspect**             | **Details**                                                                     |
|------------------------|---------------------------------------------------------------------------------|
| **Compatibility**      | Compatible with Windows. Tested with Meta Quest 3 and SteamLink.                |
| **Version Control**    | Use Git. Repository hosted on GitHub and GitLab, access granted to GabrielHare. |
| **Assets and Credits** | Document all third-party assets and code.                                       |
| **Timeframe**          | Complete the project within 4 days.                                             |

## Software Interfaces

```
Scripts/
│
├── UI/
│   └── CenterScreenUIInteractExperimental.cs
│
├── Networking/
│   ├── RpcTest.cs
│   ├── DataSyncController.cs
│   └── ConnectionController.cs
│
└── Test/
    └── TestNetworkingWithoutVR.cs
```

### ConnectionController (Networking)

- **Purpose**: Manages network connections for multiplayer interactions.
- **Key Functions**:
    - `OnInvite()`: Initiates hosting a game session using Unity's Relay Service. Generates a join code for others to
      connect.
    - `OnJoin()`: Enables a client to join a hosted game using a provided join code.
    - `StartHostWithRelay()`: Sets up and starts a game host with Unity services and Vivox for voice communications,
      returning a join code.
    - `StartClientWithRelay()`: Joins a game session using the provided join code and initializes necessary services.

### DataSyncController (Networking)

- **Purpose**: Synchronizes player data, specifically regarding player movement and position in the VR environment.
- **Key Functions**:
    - `OnNetworkSpawn()`: Initializes and identifies local player components like head and hands.
    - `Update()`: Constantly updates to synchronize player movements.
    - `SyncTransforms()`: Synchronizes the position and rotation of the player's head and hands across the network.

### CenterScreenUIInteractExperimental (UI)

- **Purpose**: Facilitates interaction with UI elements in a VR environment.
- **Key Functions**:
    - `Start()`: Initializes a virtual mouse for UI interaction.
    - `Update()`: Listens for mouse input to update UI interactions.
    - `InitializeVirtualMouse()`: Creates a virtual mouse to interact with the UI at a specified distance.
    - `UpdateRaycast()`: Casts a ray from the virtual mouse to interact with UI elements, enabling click and selection
      functionalities.

## User Interfaces

```
UI/
│
├── CanvasWorldSpaceN
│   └── ... # Has buttons for lobby creation and joining
│
└── CanvasNoVRUI
    └── ... # Same thing as CanvasWorldSpaceN, but no VR UI
```

## Milestones

- ~~**Unity Netcode for GameObjects (NGO)**: Networking functionality established.~~
- ~~**Unity Relay Service**: Implemented for P2P connection join code generation.~~
- ~~**SteamVR Integration**: Fully integrated and Steam Link compatible.~~
- ~~**Vivox Voice Communications**: Incorporated, slight delay noted in lobby join process.~~
- ~~**Player Head Synchronization**: Functional, preparing for hand movement integration.~~
- ~~**Complete Hand Synchronization**: Hand movement synchronization developed and tested.~~
- ~~**SteamVR Interaction Toolkit**: Introduced teleportation and basic interactive elements.~~
- **Virtual Desktop**: Currently in the conceptualization phase.
- **OpenAI GPT-4V API Integration**: Targeted for future implementation to enhance user interactions.
- **Better User Interface**: Aiming to upgrade from the existing provisional interface.
- **Design Scene**: Development of an improved initial VR environment underway.

## Implementation Decisions

I think it's best to avoid Steam P2P for now as that would require the build to be uploaded to Steam servers for
testing.

For networking Unity NGO is new and should be standard unless there is a good reason to use Steam. Downside is Unity
Relay has scaling costs. Relay is used for generating a lobby join code.

Vivox for voice comms seemed easiest out of other voice communication frameworks to implement and it's provided by
Unity.

No public solutions found for a virtual desktop. Will need to design this.

SteamVR plugin was used for the controller/hand interactions but Valve's code is a labyrinth and would take me a full
day to understand each class.

## Dependencies

[link](dependencies.md)