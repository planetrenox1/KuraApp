| Name | Unity Version | Spec Overview   | Dependencies            |
|------|---------------|-----------------|-------------------------|
| TBD  | 2023.2.3f1    | [link](docs/spec.md) | [link](docs/dependencies.md) |

<img src="docs/images/2024-01-04.png" width="200" alt="assistant-chan">


###### The latest playable build is in the `/builds` folder. Download `SteamLink` app from the appstore in your headset and connect to your steam client on your Windows PC via SteamLink. When connected to your PC via SteamLink run the latest build .exe from the PC.

###### To click on the UI, look at the button with the center of your VR screen then click mouse0.
